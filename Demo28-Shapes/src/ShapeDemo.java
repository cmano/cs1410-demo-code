public class ShapeDemo {
    public static void main(String[] args) {
        Circle obj1 = new Circle(2.5, "blue", false);
        System.out.println(obj1);
        System.out.println(obj1.getPerimeter());

        Rectangle obj2 = new Rectangle(3.5, 6.5, "green", true);
        System.out.println(obj2);
        System.out.println(obj2.getPerimeter());
    }
}
