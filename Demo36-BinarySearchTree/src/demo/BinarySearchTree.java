package demo;

public class BinarySearchTree<E extends Comparable> {
    private Node<E> root;

    public void insert(E value) {
        if (root == null) {
            root = new Node<>(value);
        } else {
            Node<E> parent = null;
            Node<E> node = root;
            while (node != null) {
                parent = node;
                if (node.value.compareTo(value) < 0) {
                    node = node.right;
                } else if (node.value.compareTo(value) > 0) {
                    node = node.left;
                } else {
                    return;
                }
            }
            Node<E> newNode = new Node<>(value);
            if (parent.value.compareTo(value) < 0) {
                parent.right = newNode;
            } else {
                parent.left = newNode;
            }
        }
    }

    public boolean search(E value) {
        boolean found = false;
        Node<E> node = root;

        while (!found && node != null) {
            System.out.println("Node Value: " + node.value);
            if (node.value.compareTo(value) == 0) {
                found = true;
            } else if (node.value.compareTo(value) < 0) {
                node = node.right;
            } else {
                node = node.left;
            }
        }
        return found;
    }

    public void displayTree() {
        if (root != null) {
            displayTree(root);
        } else {
            System.out.println("The tree is empty");
        }
    }

    private void displayTree(Node node) {
        if (node != null) {
            displayTree(node.left);
            displayTree(node.right);
            System.out.println("Value: " + node.value);

        }
    }

    public void remove(E value) {
        // Start by finding the node and parent of the node to delete
        Node<E> parent = null;
        Node<E> node = root;
        boolean done = false;
        while (!done) {
            if (node.value.compareTo(value) < 0) {
                parent = node;
                node = node.right;
            } else if (node.value.compareTo(value) > 0) {
                parent = node;
                node = node.left;
            } else {
                done = true;
            }
        }
        // Case for no left child
        if (node.left == null) {
            if (parent == null) {
                root = node.right;
            } else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                } else {
                    parent.left = node.right;
                }
            }
        } else { // Case for left child
            Node<E> parentOfRight = node;
            Node<E> rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            } else {
                parentOfRight.left = rightMost.left;
            }
        }
    }


    private class Node<E> {
        public E value;
        public Node left;
        public Node right;

        public Node(E o) {
            this.value = o;
        }
    }


}
