package demo;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();

        for(int i = 0; i < 200; i++) {
            tree.insert((int)(Math.random()*400));
        }

        boolean done = false;

        Scanner input = new Scanner(System.in);
        while(!done) {
            System.out.print("Guess a number: ");
            int guess = input.nextInt();

            done = tree.search(guess);
        }

        System.out.println("You finally guessed it");

        tree.displayTree();
        tree.remove(50);
    }
}
