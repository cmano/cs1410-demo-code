package demo.shape;

public class Circle {
    private double radius;
    private static int numberCreated = 0;

    public Circle() {
        setRadius(1);
        numberCreated++;
    }

    public Circle(double radius) {
        setRadius(radius);
        numberCreated++;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return Math.PI * radius * 2;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        if (radius > 0) {
            this.radius = radius;
        } else {
            // set radius to 1 if negative number is given
            this.radius = 1;
        }
    }

    public double getDiameter() {
        return radius * 2;
    }

    public void setDiameter(double diameter) {
        if (diameter > 0) {
            radius = diameter / 2.0;
        }
    }

    public static int getNumberCreated() {
        return numberCreated;
    }
}

