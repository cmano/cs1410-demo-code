/**
 * This program is used to demonstrate computing a
 * mathematical expression in Java.
 */
public class SimpleProgram {
    public static void main(String[] args) {
        System.out.print("(10.5 + 2 * 3) / (45 - 3.5) = ");
        System.out.println((10.5 + 2 * 3) / (45 - 3.5));
    }
}
