package demo;

public class RandomDemo {
    public static void main(String[] args) {
        // [0.0, 1.0)
        System.out.println(Math.random());

        // [0.0, 10.0)
        System.out.println((int)(Math.random() * 10));

        // [50.0, 100.0)
        System.out.println(50 + (int)(Math.random() * 50));

    }
}
