package demo;

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = createArr();
        bubbleSort(arr);

        for(int i : arr) {
            System.out.println(i);
        }
    }

    public static void bubbleSort(int data[]) {
        boolean didSwap = false;

        do {
            didSwap = false;
            for (int item = 0; item < (data.length - 1); item++) {
                if (data[item] > data[item + 1]) {
                    int temp = data[item];
                    data[item] = data[item + 1];
                    data[item + 1] = temp;
                    didSwap = true;
                }
            }
        } while (didSwap);
    }

    public static int[] createArr() {
        int[] arr = new int[30];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * 1000);
        }

        return arr;
    }
}
