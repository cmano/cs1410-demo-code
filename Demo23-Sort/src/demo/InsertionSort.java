package demo;

public class InsertionSort {
    public static void main(String[] args) {
        int[] arr = createArr();
        insertionSort(arr);

        for(int i : arr) {
            System.out.println(i);
        }
    }

    public static void insertionSort(int data[]) {
        for(int currPos = 1; currPos < data.length; currPos++) {
            int currElement = data[currPos];
            int sortedPos = currPos - 1;

            while(sortedPos >= 0 && data[sortedPos] > currElement) {
                data[sortedPos + 1] = data[sortedPos];
                sortedPos--;
            }

            data[sortedPos + 1] = currElement;
        }
    }

    public static int[] createArr() {
        int[] arr = new int[30];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * 1000);
        }

        return arr;
    }
}
