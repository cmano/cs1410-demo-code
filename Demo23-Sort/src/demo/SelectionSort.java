package demo;

public class SelectionSort {
    public static void main(String[] args) {
        int[] arr = createArr();
        selectionSort(arr);

        for(int i : arr) {
            System.out.println(i);
        }
    }

    public static void selectionSort(int data[]) {
        for (int start = 0; start < (data.length - 1); start++) {
            int minPos = start;
            for (int scan = start + 1; scan < data.length; scan++) {
                if (data[scan] < data[minPos]) {
                    minPos = scan;
                }
            }
            int temp = data[start];
            data[start] = data[minPos];
            data[minPos] = temp;
        }
    }

    public static int[] createArr() {
        int[] arr = new int[30];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * 1000);
        }

        return arr;
    }
}
