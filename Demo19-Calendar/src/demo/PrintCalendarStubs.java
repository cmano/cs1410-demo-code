package demo;

import java.util.Scanner;

public class PrintCalendarStubs {
    public static void main(String[] args) {
        int year = 2018;
        int month = 1;

        printMonth(year, month);
    }

    public static boolean isLeapYear(int year) {
        return true;
    }

    public static int getNumberOfDaysInMonth(int year, int month) {
        return 31;
    }

    public static int getTotalNumberOfDays(int year, int month) {
        return 10000;
    }

    public static int getStartDay(int year, int month) {
        return 1;
    }

    public static void printMonthBody(int year, int month) {
    }

    public static String getMonthName(int month) {
        return "January";
    }

    public static void printMonthTitle(int year, int month) {
    }

    public static void printMonth(int year, int month) {
    }
}
