public class DataTypes {
    public DataTypes() {
        byte maxByteVal= 127;
        short maxShortVal = 32767;
        int maxIntVal = 2147483647;
        long maxLongVal = 9_223_372_036_854_775_807L;
        double doubleVal = 1.88; //64-bit
        float floatVal = 1.88F; //32-bit
        boolean booleanVal = true;
        char charVal = 'A';

        // Print out a variable
        System.out.println(maxLongVal);
    }

    public static void main(String[] args) {
        new DataTypes();
    }
}
