public class If {
    public static void main(String[] args) {
        int x = 5;

        if (x < 10) {
            System.out.println("X is less than 10");
        }
        System.out.println("This prints no matter what");
    }
}
