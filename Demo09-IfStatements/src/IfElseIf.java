public class IfElseIf {
    public static void main(String[] args) {
        int x = 5;

        if (x < 10) {
            System.out.println("X is less than 10");
        } else if (x > 20) {
            System.out.println("X is greater than 20");
        } else {
            System.out.println("X is neither less than 10 or greater than 20");
        }
        System.out.println("This prints no matter what");
    }
}
