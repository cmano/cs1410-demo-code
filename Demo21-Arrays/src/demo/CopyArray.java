package demo;

public class CopyArray {

    public static void main(String[] args) {
        loopCopy();
        methodCopy();
    }

    public static void loopCopy() {
        int[] myList1 = { 1, 1, 2, 3, 5, 8, 13, 21 };
        int[] myList2 = new int[myList1.length];

        for (int i = 0; i < myList1.length; i++) {
            myList2[i] = myList1[1];
        }
    }

    public static void methodCopy() {
        int[] myList1 = { 1, 1, 2, 3, 5, 8, 13, 21 };
        int[] myList2 = new int[myList1.length];

        System.arraycopy(myList1, 0, myList2, 0, myList1.length);
    }

}
