package demo;

public class ProcessArray {
    public static void main(String[] args) {
        int[] intArr1 = new int[10];

        for(int i = 0; i < intArr1.length; i++) {
            intArr1[i] = (int)(Math.random() * 10);
        }

        for(int i : intArr1) {
            System.out.println("value: " + i);
        }
    }
}
