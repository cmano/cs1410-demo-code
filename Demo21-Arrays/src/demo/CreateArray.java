package demo;

public class CreateArray {
    public static void main(String[] args) {
        //Create with size
        int[] intArr2 = new int[10];
        double[] doubleArr2 = new double[10];

        // Create and initialize with values
        int[] intArr1 = {1, 2, 3, 4, 5};
        double[] doubleArr1 = {1.1, 2.2, 3.3, 4.4};

        int intArr[] = {1, 2, 3};
    }
}
