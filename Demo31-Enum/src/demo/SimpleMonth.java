/**
 * 
 */
package demo;

/**
 * @author Chad
 *
 */
public enum SimpleMonth {
	JANUARY,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER;
}
