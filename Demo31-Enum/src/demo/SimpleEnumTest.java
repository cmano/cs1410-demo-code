/**
 * 
 */
package demo;

/**
 * @author Chad
 *
 */
public class SimpleEnumTest {
	SimpleMonth month;
	
	public SimpleEnumTest(SimpleMonth month) {
		this.month = month;
	}
	
	public void printStuff() {
		switch(month) {
		case JANUARY:
			System.out.println("It's Cold Outside");
			break;
		case FEBRUARY:
			System.out.println("Happy Valentine's Day");
			break;
		default:
			System.out.println("I can't type that much");
			break;
		}
	}

	public static void main(String[] args) {
		SimpleEnumTest mon1 = new SimpleEnumTest(SimpleMonth.JANUARY);
		mon1.printStuff();
	}

}
