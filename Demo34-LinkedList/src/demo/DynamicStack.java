package demo;

public class DynamicStack<E> {
    private StackNode top;

    private class StackNode<E> {
        public E value;
        public StackNode<E> next;

        public StackNode(E v) {
            this.value = v;
        }
    }

    public static void main(String[] args) {
        DynamicStack<Integer> dynamicStack = new DynamicStack<>();

        for(int i = 0; i < 50; i++) {
            try {
                dynamicStack.push(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        while(true) {
            try {
                System.out.println(dynamicStack.pop());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public DynamicStack() {

    }

    public void push(E v) throws Exception {
        StackNode<E> newTop = new StackNode<>(v);

        newTop.next = top;
        top = newTop;
    }

    public E pop() throws Exception
    {
        if(isEmpty()) {
            throw new Exception("The Stack is Empty");
        }

        StackNode<E> oldTop = top;
        top = oldTop.next;
        return oldTop.value;
    }

    public boolean isEmpty() {
        return this.top == null;
    }
}
