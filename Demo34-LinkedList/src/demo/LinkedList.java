package demo;

public class LinkedList<E extends Comparable> {
    private Node<E> head = new Node<E>();
    private Node<E> tail;
    private int size;

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.insert(5);
        list.insert(4);
        list.insert(10);
        list.insert(8);
        list.printList(list);

        System.out.println("\n" + list.size + "\n");

        list.delete(4);
        list.printList(list);

        System.out.println("\n" + list.size);
    }

    public LinkedList() {
        this.size = 0;
    }

    public void insert(E o) {
        Node<E> node = new Node<>(o);
        Node<E> current = head.next;
        Node<E> previous = head;

        while(current != null && current.value.compareTo(o) < 0) {
            previous = current;
            current = current.next;
        }

        previous.next = node;
        node.next = current;

        if(current == null) {
            tail = node;
        }

        size++;
    }

    public void delete(E val) {
        Node<E> current = head.next;
        Node<E> previous = head;

        while(current.next != null && current.value!= val) {
            previous = current;
            current = current.next;
        }

        previous.next = current.next;

        if(previous.next == null) {
            tail = previous;
        }

        size--;
    }

    public boolean find(E o) {
        Node<E> node = head.next;
        boolean found = false;

        while(!found && node != null) {
            if(node.value == o) {
                found = true;
            } else {
                node = node.next;
            }
        }
        return found;
    }

    public void append(E o) {
        Node<E> node = new Node<>(o);

        if(tail == null) {
            head.next = tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        this.size++;
    }

    public void printList(LinkedList list) {
        Node<E> current = head.next;

        while(current != null) {
            System.out.println(current.value);
            current = current.next;
        }
    }

    public int getSize() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    private class Node<E> {
        public E value;
        public Node<E> next;

        public Node() {

        }

        public Node(E value) {
            this.value = value;
        }
    }
}

