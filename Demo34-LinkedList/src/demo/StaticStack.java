package demo;

public class StaticStack<E> {
    private E[] data;
    private int top;

    public static void main(String[] args) {
        StaticStack<Integer> staticStack = new StaticStack<>(50);

        for(int i = 0; i < 50; i++) {
            try {
                staticStack.push(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        while(true) {
            try {
                System.out.println(staticStack.pop());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public StaticStack(int maxSize) {
        this.data = (E[])new Object[maxSize];
        this.top = -1;
    }

    public void push(E v) throws Exception {
        if(isFull()) {
            throw new Exception("The stack is full");
        }

        this.top++;
        this.data[top] = v;
    }

    public E pop() throws Exception
    {
        if(isEmpty()) {
            throw new Exception("The stack is empty");
        }

        return this.data[top--];
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public boolean isFull() {
        return this.top == this.data.length - 1;
    }
}
