/**
 * Make sure to set up your testing libraries to get this
 * to work properly.
 */

package demo;

import org.junit.Assert;

public class TestMethods {

    @org.junit.Test
    public void isPositive() {
        Assert.assertTrue(TestDemo.isPositive(10));
        Assert.assertFalse(TestDemo.isPositive(-10));
        Assert.assertTrue(TestDemo.isPositive(0));
    }

    @org.junit.Test
    public void isWhole() {
        Assert.assertTrue(TestDemo.isWhole(1.0));
        Assert.assertFalse(TestDemo.isWhole(1.123));
    }

    @org.junit.Test
    public void addValues() {
        Assert.assertEquals(TestDemo.addValues(5, 2), 7);
    }
}