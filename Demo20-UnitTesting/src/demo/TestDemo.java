package demo;

public class TestDemo {
    public static void main(String[] args) {

    }

    public static boolean isPositive(double number) {
        return number >= 0;
    }

    public static boolean isWhole(double value) {
        double wholePart = Math.floor(value);
        return wholePart == value;
    }

    public static int addValues(int val1, int val2) {
        return val1 + val2;
    }
}
