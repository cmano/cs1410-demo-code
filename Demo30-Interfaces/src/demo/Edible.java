package demo;

public interface Edible {
    String howToEat();
}
