package demo;

public class Orange extends Fruit {
    public Orange(String color) {
        super(color);
    }

    @Override
    public String howToEat() {
        return "Orange: Peel first, then bite";
    }
}
