package demo;

public class Apple extends Fruit implements Comparable<Apple> {
    public Apple(String color) {
        super(color);
    }

    @Override
    public String howToEat() {
        return "Apple: Just take a bite";
    }

    @Override
    public int compareTo(Apple o) {
        if(this.getColor().equals(o.getColor())) {
            return 0;
        }
        else
            return -1;
    }
}
