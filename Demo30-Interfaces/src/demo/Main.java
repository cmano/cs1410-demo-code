package demo;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Animal> animalList = new ArrayList<>();
        ArrayList<Fruit> fruitList = new ArrayList<>();
        ArrayList<Edible> edibleList = new ArrayList<>();

        animalList.add(new Tiger(500));
        animalList.add(new Chicken(3));
        animalList.add(new Tiger(1500));
        animalList.add(new Chicken(3));

        if(((Chicken)animalList.get(1)).compareTo(((Chicken)animalList.get(3))) == 0) {
            System.out.println("Chickens are the same size");
        }

        fruitList.add(new Orange("Orange"));
        fruitList.add(new Apple("Red"));
        fruitList.add(new Orange("Yellow"));
        fruitList.add(new Apple("Green"));

        edibleList.add(new Orange("Orange"));
        edibleList.add(new Chicken(1.5));
        edibleList.add(new Apple("Red"));
    }
}
