package demo;

public class Chicken extends Animal implements Edible, Comparable<Chicken> {
    public Chicken(double weight) {
        super(weight);
    }

    @Override
    public String sound() {
        return "Cluck-cluck";
    }

    @Override
    public String howToEat() {
        return "Chicken: Fry it up";
    }

    @Override
    public int compareTo(Chicken o) {
        if(this.getWeight() == o.getWeight()) {
            return 0;
        } else if(this.getWeight() > o.getWeight()) {
            return 1;
        } else {
            return -1;
        }
    }
}
