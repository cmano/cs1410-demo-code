package demo;

public class SquareArray {
    public static void main(String[] args) {
        int[][] square = new int[5][5];
        int fillValue = 1;
        for (int[] row : square) {
            java.util.Arrays.fill(row, fillValue++);
            for (int element : row) {
                System.out.printf("%d ", element);
            }
            System.out.println();
        }

        System.out.println();

        for (int row = 0; row < square.length; row++) {
            for (int column = 0; column < square[row].length; column++) {
                System.out.printf("%d ", square[row][column]);
            }
            System.out.println();
        }

    }
}
