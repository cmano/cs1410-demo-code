package demo;

public class JaggedArray {
    public static void main(String[] args) {
        int[][] ragged = new int[5][];
        ragged[0] = new int[5];
        ragged[1] = new int[4];
        ragged[2] = new int[3];
        ragged[3] = new int[2];
        ragged[4] = new int[1];

        int fillValue = 1;
        for (int[] row : ragged) {
            java.util.Arrays.fill(row, fillValue++);
            for (int element : row) {
                System.out.printf("%d ", element);
            }
            System.out.println();
        }
    }
}
