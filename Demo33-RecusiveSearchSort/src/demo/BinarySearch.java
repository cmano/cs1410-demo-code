package demo;

public class BinarySearch {
    public static void main(String[] args) {
        int[] sortMe = Utilities.createArr();
        SelectionSort.selectionSort(sortMe, 0, sortMe.length - 1);

        if(binarySearch(sortMe, 15)) {
            System.out.println("The value 15 was found");
        } else {
            System.out.println("The value 15 was not found");
        }
    }

    public static boolean binarySearch(int[] data, int search) {
        return binarySearch(data, 0, data.length - 1, search);
    }

    public static boolean binarySearch(int[] data, int low, int high, int search) {
        if (low > high) return false;

        int middle = (low + high) / 2;
        if (data[middle] == search) {
            return true;
        }
        else if (data[middle] < search) {
            return binarySearch(data, middle + 1, high, search);
        }
        else {
            return binarySearch(data, low, middle - 1, search);
        }
    }

}
