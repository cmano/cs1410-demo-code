package demo;

public class QuickSort {
    public static void main(String[] args) {
        int[] data = Utilities.createArr();
        quickSort(data, 0, data.length - 1);

        for(int i : data) {
            System.out.println(i);
        }
    }

    public static void quickSort(int[] data, int start, int end) {
        if(start < end) {
            int pivot = partition(data, start, end);
            quickSort(data, start, pivot - 1);
            quickSort(data, pivot + 1, end);
        }
    }

    private static int partition(int[] data, int start, int end) {
        int middle = (start + end) / 2;
        int tmp = data[start];
        data[start] = data[middle];
        data[middle] = tmp;

        int index = start;
        int pivotValue = data[start];

        for(int scan = start + 1; scan <= end; scan++) {
            if(data[scan] < pivotValue) {
                index++;
                tmp = data[index];
                data[index] = data[scan];
                data[scan] = tmp;
            }
        }

        tmp = data[start];
        data[start] = data[index];
        data[index] = tmp;

        return index;
    }
}
