package demo;

public class SelectionSort {
    public static void main(String[] args) {
        int[] sortMe = Utilities.createArr();
        selectionSort(sortMe, 0, sortMe.length - 1);
        for (int value : sortMe) {
            System.out.printf("%d, ", value);
        }
    }

    public static void selectionSort(int[] data, int low, int high) {
        if (low < high) {
            int minPos = low;
            for (int scan = low + 1; scan <= high; scan++) {
                if (data[scan] < data[minPos]) {
                    minPos = scan;
                }
            }

            int temp = data[low];
            data[low] = data[minPos];
            data[minPos] = temp;

            selectionSort(data, low + 1, high);
        }
    }
}
