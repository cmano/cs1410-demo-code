package demo;

public class PassByValue {
    public PassByValue() {
        int x = 5;
        int y = 10;
        swapDoesntWork(x, y);
        System.out.println("x: " + x);
        System.out.println("y: " + y);

        y = swapDoesWork(x, x = y);
        System.out.println("x: " + x);
        System.out.println("y: " + y);
    }

    private void swapDoesntWork(int x, int y) {
        int tmp = x;
        x = y;
        y = tmp;
    }

    private int swapDoesWork(int x, int y) {
        System.out.println(y);
        return x;
    }

    public static void main(String[] args) {
        new PassByValue();
    }
}
