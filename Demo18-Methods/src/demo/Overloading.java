package demo;

public class Overloading {
    public static void main(String[] args) {
        // Change the parameters
        System.out.println(max(5, 2));
    }

    public static int max(int x, int y) {
        System.out.println("int, int");
        if (x > y) return x;

        return y;
    }

    public static double max(double x, double y) {
        System.out.println("double, double");
        if (x > y) return x;

        return y;
    }

    public static double max(int x, double y) {
        System.out.println("int, double");
        if (x > y) return x;

        return y;
    }

    public static double max(double x, double y, double z) {
        System.out.println("double, double, double");
        return max(max(x, y), z);
    }

}
