package demo;

public class MethodsDemo {
    // Data members are declared inside the class, but outside
    // a method or constructor.
    private int instanceVariable = 50;

    public int getInstanceVariable() {
        return instanceVariable;
    }

    public static void main(String[] args) {
        MethodsDemo demo = new MethodsDemo();
        System.out.print("Methods can access data members: ");
        System.out.println(demo.getInstanceVariable());
    }
}
