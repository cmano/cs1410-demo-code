package demo;

import java.lang.Math; // Not actually necessary. Why?
import java.util.*; // Wildcards are not a good idea
import static java.lang.Math.pow; // A static import

public class MathDemo {
    public static void main(String[] args) {
        // Use Math.* to use a method in Math
        System.out.println(Math.round(1432.123));

        // A static import let's you just use the method
        System.out.println(pow(5, 2));

    }
}
