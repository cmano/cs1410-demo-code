import java.util.Scanner;

/**
 * I purposely do not add comments so you can read the code and figure out what's happening.
 *
 * @author Chad
 * @version XXX
 */
public class ConsoleScannerDemo {
    Scanner input;
    public ConsoleScannerDemo() {
        int intVal;
        String stringVal;
        double doubleVal;

        input = new Scanner(System.in);

        System.out.print("Enter an int: ");
        intVal = input.nextInt();

        System.out.print("Enter a string: ");
        stringVal = input.next();

        System.out.print("Enter a double: ");
        doubleVal = input.nextDouble();

        System.out.printf("You entered: %d %s %f", intVal, stringVal, doubleVal); //printf example
    }


    public static void main(String args[]){
        new ConsoleScannerDemo();
    }
}
