package demo;

/**
 * Add a description of the class here
 *
 * @author Chad
 * @version XXX
 */
public class StringDemo {

    public StringDemo() {
        String a = "Hello";
        String b = "There";
        String c = new String("Goodbye");
        int d = 5;
        int e = 3;

        System.out.println(a + " " + b);
        System.out.println(d + e);
        System.out.println("Sum: " + d + e);
        System.out.println(a.length());
        System.out.println(a.charAt(4));
        System.out.println(a.indexOf('l'));
    }

    public static void main(String args[]) {
        new StringDemo();
    }
}