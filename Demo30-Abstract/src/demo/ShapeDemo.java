package demo;

import java.util.ArrayList;

public class ShapeDemo {
    public static void main(String[] args) {
        ArrayList<GeometricObject> list = new ArrayList<>();
        list.add(new Circle(1, "blue", true));
        list.add(new Rectangle(1, 1 * Math.PI, "green", false));
        list.add(new Circle(2.5, "yellow", true));
        list.add(new Rectangle(1.5, 19.5, "red", false));

        System.out.println(equalArea(list.get(0), list.get(1)));
    }

    public static boolean equalArea(GeometricObject obj1, GeometricObject obj2) {
        return obj1.getArea() == obj2.getArea();
    }

}
