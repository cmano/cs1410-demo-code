package demo;

public class Trapezoid extends GeometricObject {
    public Trapezoid(String color, boolean filled) {
        super(color, filled);
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }
}
