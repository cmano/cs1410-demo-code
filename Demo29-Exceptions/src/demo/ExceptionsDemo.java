package demo;

import java.util.Scanner;

public class ExceptionsDemo {
    public static void main(String[] args) {
        try {
            Circle myCircle = new Circle(1);
            Circle myOtherCircle = new Circle(-4);

            System.out.println("MyCircle area: " + myCircle.getArea());
            System.out.println("MyOtherCircle area: " + myOtherCircle.getArea());
        }
        catch (InvalidRadiusException ex) {
            ex.getRadius();
            System.out.println("Error: " + ex.getMessage());
        }

//        Scanner input = new Scanner(System.in);
//
//        System.out.print("Enter two integers: ");
//
//        try {
//            int number1 = input.nextInt();
//            int divisor = input.nextInt();
//
//            System.out.printf("%d / %d = %d\n",
//                    number1,
//                    divisor,
//                    quotient2(number1, divisor));
//        }
//        catch (java.lang.IllegalArgumentException ex) {
//            System.out.println("Error: " + ex.getMessage());
//        }
//        catch (java.lang.ArithmeticException ex) {
//            System.out.println("Probably a divide by 0 error");
//        }
//        catch (java.util.InputMismatchException ex) {
//            System.out.println("There was an error with the input");
//        }
//        catch (java.lang.Exception ex) {
//            System.out.println("You are a bad person for causing this");
//        }
//
//        System.out.println("This better always run!");
    }

    public static void quotient() {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter two integers: ");
        int number1 = input.nextInt();
        int divisor = input.nextInt();

        if (divisor != 0) {
            System.out.printf("%d / %d = %d\n",
                    number1,
                    divisor,
                    number1 / divisor);
        } else {
            System.out.println("Divisor can not be 0");
        }
    }

    public static int quotient2(int x, int y) throws java.lang.IllegalArgumentException {
        if (y == 0) {
            java.lang.IllegalArgumentException ex = new java.lang.IllegalArgumentException("divisor is 0");
            throw ex;
        }
        return x / y;
    }
}

class InvalidRadiusException extends Exception {
    private double radius;

    public InvalidRadiusException(double radius) {
        super("Invalid radius: " + radius);
        this.radius = radius;
    }

    public double getRadius() { return this.radius; }
}

class Circle {
    private double radius;

    public Circle(double radius) throws InvalidRadiusException {
        if (radius < 0) {
            throw new InvalidRadiusException(radius);
        }
        this.radius = radius;
    }

    public double getRadius() { return this.radius; }
    public double getArea() { return Math.PI * this.radius * this.radius; }
    public double getPerimeter() { return 2 * Math.PI * this.radius; }
}
