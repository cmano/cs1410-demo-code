package demo;

public class InterningDemo {
    public static void main(String[] args) {
        String str1 = "Hello";
        String str2 = "Hello";
        String str3 = new String("Hello");

        // Compare with ==
        System.out.println(str1 == str2);
        System.out.println(str1 == str3);

        // Intern explicitly
        String str4 = new String("Hello").intern();
        System.out.println(str1 == str4);

        // Remember, == compares reference values, not String content
    }
}
