package src.demo;

import java.io.*;
import java.util.Scanner;

/**
 * Add a description of the class here
 *
 * @author Chad
 * @version XXX
 */
public class FileIO {
    //Note: The filename includes the module name because of the way I have
    // code organized. In your project you will not need the project name.
    private static final String fileName = "Demo35-FileIO/data/info.data";

    public FileIO() {
        fileRead();
        fileWrite();
    }

    private void fileRead() {
        Scanner fileInput = null;

        try {
            fileInput = new Scanner(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (fileInput.hasNext()) {
            System.out.println(fileInput.nextLine());
        }
        fileInput.close();
    }

    private void fileWrite() {
        PrintWriter fileOutput = null;

        try {
            fileOutput = new PrintWriter(new File(fileName)); //try changing to false
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        fileOutput.println("Print this!");
        fileOutput.println("Print this!");
        fileOutput.println("Print this!");
        fileOutput.close();
    }

    public static void main(String args[]) {
        new FileIO();
    }
}
