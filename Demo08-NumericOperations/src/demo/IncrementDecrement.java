package demo;

public class IncrementDecrement {
    public static void main(String[] args) {
        int val1 = 0;
        int val2 = 0;

        System.out.println("Val1: " + val1);
        System.out.println("Val1++: " + val1++);
        System.out.println("Val1: " + val1);

        System.out.println("\nVal2: " + val2);
        System.out.println("++Val2: " + ++val2);
        System.out.println("Val2: " + val2);
    }
}
