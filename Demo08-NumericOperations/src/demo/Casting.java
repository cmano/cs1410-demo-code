package demo;

public class Casting {
    public static void main(String[] args) {
        int intVal = 3941;
        double doubleVal = 481.38291;

        // byte byteVal = intVal; // This doesn't work
        byte byteVal = (byte)intVal;
        System.out.println(byteVal);

        // short shortVal = doubleVal; // This doesn't work
        short shortVal = (short)doubleVal;
        System.out.println(shortVal);

        long longVal = intVal; // This works!
        System.out.println(longVal);
    }
}
