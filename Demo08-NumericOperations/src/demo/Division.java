package demo;

public class Division {
    public static void main(String[] args) {
        int numerator = 5;
        int intDenominator = 2;
        double floatingPointDenominator = 2.0;

        System.out.print("Integer Division: ");
        System.out.println(numerator / intDenominator);

        System.out.print("Floating Point Division: ");
        System.out.println(numerator / floatingPointDenominator);

        System.out.print("Casting Division: ");
        System.out.println((float)numerator / intDenominator);

    }
}
