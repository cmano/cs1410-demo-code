package demo;

public class Literals {
    public static void main(String[] args) {
        // Integral Literals
        int value1 = 2;
		int value2 = 0b11;	// Binary 3
		int value3 = 011;	// Octal 9
		int value4 = 0x11;	// Hexadecimal 17
		// Java allows _ (underscore) between two digits to improve readability
		int value5 = 123_000 + 123;


		// Floating-Point Literals
		double value6 = 2.0;	// By default, floating-point literals are double
		double value7 = 2.0d;	// Can explicitly mark as double
        float value8 = 2.0f;	// Add an ‘f’ to the end of a literal to make it a float

        // Print out the values to see what they do
        System.out.println(value2);
    }
}
