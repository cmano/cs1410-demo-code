package demo;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(12));
        System.out.println(factorialTail(12, 1));
    }

    public static long factorial(int n) {
        if (n == 0) {
            return 1;
        }
        else {
            return n * factorial(n - 1);
        }
    }

    public static long factorialTail(int n, int result) {
        if (n == 0) {
            return result;
        }
        return factorialTail(n - 1, n * result);
    }


}
