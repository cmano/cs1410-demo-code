package demo;

public class CharacterDemo {
    public static void main(String[] args) {
        char char1 = 'a'; // Use single quotes
        Character char2 = 'b'; // Can use a Wrapper class

        // System.out.println(char1.???); // No methods on a primitive type

        System.out.println(char2.compareTo('c')); // Wrapper class object has methods

        // Use static methods in Character
        System.out.println(Character.isDigit(char1));
    }
}
