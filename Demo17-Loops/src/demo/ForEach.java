package demo;

public class ForEach {
    public static void main(String[] args) {
        int[] intArr = {5, 4, 3, 2, 1};
        for (int i : intArr) {
            System.out.println(i); //don't do intArr[i]
        }


        //notice the default values
        int[] intArr2 = new int[5];
        for (int i : intArr2) {
            System.out.println(i); //don't do intArr2[i]
        }


        //notice the default values
        Number[] numArr = new Number[10];
        numArr[0] = new Integer(5);
        numArr[2] = new Double(123.321);
        numArr[5] = new Float(32.12);
        numArr[9] = new Integer(33);

        for (Number i : numArr) {
            System.out.println(i); //don't do numArr[i]
        }
    }
}
