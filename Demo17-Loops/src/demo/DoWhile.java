package demo;

import java.util.Scanner;

public class DoWhile {
    final static int doneVal = 5;

    public static void main(String[] args) {
        System.out.println("A Sentinel Value");
        boolean done = false;
        do {
            System.out.println("I'm not done!");
            done = (int)(Math.random() * 10) == doneVal;
        } while(!done);

        System.out.println("\nA Counter");
        int cnt = 0;
        do {
            System.out.println("I'm not done!");
            cnt++;
        } while(cnt < doneVal);

        System.out.println("\nA break");
        do {
            System.out.println("I'm not done!");
            if((int)(Math.random() * 10) == doneVal) {
                break;
            }
        } while(false);

        System.out.println("\nUser Input");
        Scanner scan = new Scanner(System.in);
        char userInput = 'A';
        do {
            System.out.println("I'm not done!");
            System.out.print("Continue Loop? (Y/N): ");
            userInput = Character.toUpperCase(scan.next().charAt(0));
        } while(userInput == 'Y');

    }
}
