package demo;

import java.util.Scanner;

public class While {
     static int doneVal = 5;

    public static void main(String[] args) {
        System.out.println("A Sentinel Value");
        boolean done = false;
        while(!done) {
            System.out.println("I'm not done!");
            done = (int)(Math.random() * 10) == doneVal;
        }

        System.out.println("\nA Counter");
        int cnt = 0;
        while(cnt < doneVal) {
            System.out.println("I'm not done!");
            cnt++;
        }

        System.out.println("\nA break");
        while(true) {
            System.out.println("I'm not done!");
            if((int)(Math.random() * 10) == doneVal) {
                break;
            }
        }

        System.out.println("\nUser Input");
        System.out.print("Continue Loop? (Y/N): ");
        Scanner scan = new Scanner(System.in);
        char userInput = Character.toUpperCase(scan.next().charAt(0));
        while(userInput == 'Y') {
            System.out.println("I'm not done!");
            System.out.print("Continue Loop? (Y/N): ");
            userInput = Character.toUpperCase(scan.next().charAt(0));
        }

    }
}
