package demo;

public class For {
    public static void main(String[] args) {
        int[] intArr = {5, 4, 3, 2, 1};
        for (int i = 0; i < intArr.length - 1; i++) {
            System.out.println(intArr[i] + intArr[i + 1]);
        }


        //notice the default values
        int[] intArr2 = new int[5];
        for (int i = 0; i < intArr2.length; i++) {
            System.out.println(intArr2[i]);
        }


        //notice the default values
        Number[] numArr = new Number[10];
        numArr[0] = new Integer(5);
        numArr[2] = new Double(123.321);
        numArr[5] = new Float(32.12);
        numArr[9] = new Integer(33);

        for (int i = 0; i < numArr.length; i++) {
            System.out.println(numArr[i]);
        }
    }
}
