package demo;

public class LinearSearch {

    public static void main(String[] args) {
        int[] intArr = {6, 53, 1, 12, 19, 77};

        int result = linearSearch(intArr, 12);
        if(result != -1) {
            System.out.println("Found at position " + result);
        } else {
            System.out.println("Not found");
        }
    }

    public static int linearSearch(int[] array, int search) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == search) {
                return i;
            }
        }
        return -1;
    }
}
