package demo;

public class BinarySearch {
    public static void main(String[] args) {
        int[] intArr = {1, 3, 5, 7, 8, 11, 14, 15, 16, 18, 20};

        int result = binarySearch(intArr, 3);
        if(result != -1) {
            System.out.println("Found at position " + result);
        } else {
            System.out.println("Not found");
        }
    }

    public static int binarySearch(int[] arr, int val) {
        int lower = 0;
        int upper = arr.length - 1;

        while(lower <= upper) {
            int mid = (upper + lower) / 2;
            if(arr[mid] == val) {
                return mid;
            } else if(arr[mid] < val) {
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }

        return -1;
    }
}
