package demo;

public class LogicalVsBitwise {
    public LogicalVsBitwise() {
        System.out.println("Logical Operators");

        // Logical 'and'
        System.out.println("true && false");
        System.out.println(testOne() && testTwo());
        System.out.println();
        System.out.println("false && true");
        System.out.println(testTwo() && testOne());
        System.out.println();

        // Bitwise 'and'
        System.out.println("true & false");
        System.out.println(testOne() & testTwo());
        System.out.println();
        System.out.println("false & true");
        System.out.println(testTwo() & testOne());
        System.out.println();

        // Logical 'or'
        System.out.println("true || false");
        System.out.println(testOne() || testTwo());
        System.out.println();
        System.out.println("false || true");
        System.out.println(testTwo() || testOne());
        System.out.println();

        // Bitwise 'or'
        System.out.println("true | false");
        System.out.println(testOne() | testTwo());
        System.out.println();
        System.out.println("false | true");
        System.out.println(testTwo() | testOne());
    }

    private boolean testOne() {
        System.out.println("Calling testOne()");
        return true;
    }

    private boolean testTwo() {
        System.out.println("Calling testOne()");
        return false;
    }

    public static void main(String[] args) {
        new LogicalVsBitwise();
    }
}
