package demo;

public class LogicalVsBitwiseTwo {
    public LogicalVsBitwiseTwo() {
        boolean a = true;
        boolean b = false;

        int x = 1234;
        int y = 4321;

        System.out.println(a && b);
        System.out.println(a & b);
//        System.out.println(x && y); // This doesn't work
        System.out.println(x & y); // This is actually doing an 'and' bit by bit with the binary values
    }

    public static void main(String[] args) {
        new LogicalVsBitwiseTwo();
    }
}
