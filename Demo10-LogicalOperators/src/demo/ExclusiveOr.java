package demo;

public class ExclusiveOr {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = true;
        boolean c = false;
        boolean d = false;

        System.out.println("true ^ false: " + (a ^ c));
        System.out.println("false ^ true: " + (c ^ a));
        System.out.println("true ^ true: " + (a ^ b));
        System.out.println("false ^ false: " + (c ^ d));
    }
}
