/**
 * This program is used to demonstrate a very simple
 * Java program.
 */
public class HelloWorld {
    private String msg = "Hello World!!!";

    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.printMsg();
    }

    public void printMsg() {
        System.out.println(msg);
    }
}
