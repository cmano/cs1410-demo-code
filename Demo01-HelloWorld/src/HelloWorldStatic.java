public class HelloWorldStatic {
    static String msg = "Hello There";

    public static void main(String[] args) {
        printMsg();
    }

    static void printMsg() {
        System.out.println(msg);
    }
}
