package cs2410.demo; //package statement goes first

import cs2410.mypackages.ImportedClass; //import statements next

/**
 * Description of the class goes here. It belongs in Javadoc comments
 * Demo class to show the structure of a java file.
 *
 * //make sure to use these tags
 * @author Chad Mano
 * @version 1.0
 */
public class MyClass{ //class code follows
    //data members go here. Use Javadoc for each member
    /**
     * private integer used for input to an expression
     */
    private int privInt = 50;

    /**
     * public double used for input to an expression
     */
    public double pubDouble = 2.5;

    /**
     * total value of our expression
     */
    public double total;

    /**
     * instance of ImportedClass
     */
    public ImportedClass importedClass = new ImportedClass();

    /**
     * constant factor for expressions
     */
    public final int INT_FACTOR;

    //constructors typically go here
    /**
     * Default constructor
     */
    public MyClass() {
        total = add(privInt, pubDouble);
        INT_FACTOR = 6;
        System.out.println(add(privInt, pubDouble));
    }

    //methods typically go here
    /**
     * Add two values together and return the total
     *
     * @param x first operand
     * @param y second operand
     * @return the sum of the input values
     */
    public double add(int x, double y) {
        return x + y;
    }

    /**
     *
     * @param args Command line arguments for the program
     */
    public static void main(String[] args){
        new MyClass();
    }
}